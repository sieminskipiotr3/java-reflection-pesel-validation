PESEL is a polish identification number.

It is an equivalent of Social Security Number in US
or BSN in the Netherlands or National Insurance Number in the UK.

This one-class application delivers solution for validating the PESEL 
according to the strictly set rules, generating birthdate and gender
based on the PESEL. 

It uses encapsulation to deliver user-friendly options for getting the data they need,
while concealing all logic and complexity behind the scenes.

Tests are run using the reflection API.

It can be used in government applications that process people's data.

You can find the all the PESEL's details here: https://en.wikipedia.org/wiki/PESEL
