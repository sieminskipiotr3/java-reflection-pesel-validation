package psieminski;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class PESELClass {

    private String pesel;
    private Date birthDate;
    private Gender gender;

    private static final String pattern = "\\d{11}";
    private static final Pattern regex = Pattern.compile(pattern);
    private static final DateFormat simpleDate = new SimpleDateFormat("dd-MM-yyyy");

    public PESELClass(String pesel) throws ParseException {
        if (validatePesel(pesel)) {
            setPesel(pesel);
            setBirthDate(extractBirthDate(pesel));
            setGender(getGender(pesel));
        }
    }

    public String getPesel() {
        return pesel;
    }

    private void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    private void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    private void setGender(Gender gender) {
        this.gender = gender;
    }

    private Boolean validatePesel(String pesel) {

        if (!regex.matcher(pesel).matches()) {
            throw new IllegalArgumentException("This PESEL does not consist of 11 digits");
        }

        String[] splitted = pesel.split("");
        int sum = 0;

        for (int i = 0; i < splitted.length - 1; i++) {
            sum += (Integer.parseInt(splitted[i]) * getMultiplier(i + 1));
        }

        int modulo = sum % 10;
        int lastD = Integer.parseInt(pesel.substring(pesel.length() - 1));

        return (modulo == 0) && lastD == 0 || lastD == 10 - modulo;

    }

    private Integer getMultiplier(int index) {
        switch (index % 4) {
            case 1 -> {
                return 1;
            }
            case 2 -> {
                return 3;
            }
            case 3 -> {
                return 7;
            }
            case 0 -> {
                return 9;
            }
        }
        throw new IllegalArgumentException("Index does not allow conversion to the multiplier");
    }

    private Date extractBirthDate(String pesel) throws ParseException {

        String[] splitted = pesel.split("");
        int monthValue = Integer.parseInt(splitted[2] + splitted[3]);
        String year = "";
        String month = "";
        String day = splitted[4] + splitted[5];
        Date birthDate;

        if (monthValue <= 12) {
            year = "19" + splitted[0] + splitted[1];
            month = splitted[2] + splitted[3];
        } else if (monthValue > 12 && monthValue <= 32) {
            year = "20" + splitted[0] + splitted[1];
            month = String.valueOf(Integer.parseInt(splitted[2] + splitted[3]) - 20);
        } /*else if (monthValue > 32 %% monthValue <= 52) {
            year = "21" + splitted[0] + splitted[1];
            month = String.valueOf(Integer.parseInt(splitted[2] + splitted[3]) - 40);
        } this is a possible implementation for people born in 22nd century.
        system allows pesel validation up until 23rd century inclusive*/

        String finalDate = day + "-" + month + "-" + year;
        birthDate = simpleDate.parse(finalDate);
        return birthDate;
    }

    private Gender getGender(String pesel) {
        String[] splitted = pesel.split("");
        int determineValue = Integer.parseInt(splitted[9]);
        if (determineValue % 2 == 0) {
            return Gender.Female;
        }
        return Gender.Male;
    }

}
