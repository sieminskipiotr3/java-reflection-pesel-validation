package psieminski.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import psieminski.Gender;
import psieminski.PESELClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PESELClassTest {

    private static final DateFormat simpleDate = new SimpleDateFormat("dd-MM-yyyy");

    private static List<String> getMethodNames(Method[] methods) {
        List<String> methodNames = new ArrayList<>();
        for (Method m : methods) {
            methodNames.add(m.getName());
        }
        return methodNames;
    }

    private static List<String> getFieldNames(Field[] fields) {
        List<String> fieldNames = new ArrayList<>();
        for (Field f : fields) {
            fieldNames.add(f.getName());
        }
        return fieldNames;
    }

    @Test
    void checkThrowsInvalidPesel() {

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new PESELClass("123456789"));

        String expected = "This PESEL does not consist of 11 digits";
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void testClassNameAndConstructorAndFieldsAndMethods() throws ClassNotFoundException {

        Class<?> peselClass = Class.forName("psieminski.PESELClass");
        Constructor<?>[] constructors = peselClass.getConstructors();
        Field[] fields = peselClass.getDeclaredFields();
        Method[] methods = peselClass.getDeclaredMethods();

        List<String> actualFields = getFieldNames(fields);
        List<String> actualMethods = getMethodNames(methods);

        assertEquals(1, constructors.length);
        assertEquals("psieminski.PESELClass", constructors[0].getName());
        assertTrue(actualFields.containsAll(Arrays.asList("pesel", "birthDate", "gender")));
        assertTrue(actualMethods.containsAll(Arrays.asList("getPesel", "setPesel"
                , "validatePesel", "getMultiplier", "extractBirthDate", "getGender")));
    }

    @Test
    void testInvokingMethodsAtRuntime() throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException, ParseException, NoSuchMethodException {

        Class<?> peselClass = Class.forName("psieminski.PESELClass");
        PESELClass testClass = (PESELClass) peselClass.getConstructors()[0].newInstance("57083005707"); //this is a randomly generated PESEL
        Method extractDateMethod = peselClass.getDeclaredMethod("extractBirthDate", String.class);
        Method getGenderMethod = peselClass.getDeclaredMethod("getGender", String.class);
        Method validateMethod = peselClass.getDeclaredMethod("validatePesel", String.class);

        extractDateMethod.setAccessible(true);
        getGenderMethod.setAccessible(true);
        validateMethod.setAccessible(true);

        var expectedDate = extractDateMethod.invoke(testClass, testClass.getPesel());
        var expectedGender = getGenderMethod.invoke(testClass, testClass.getPesel());
        var expectedValidation = validateMethod.invoke(testClass, testClass.getPesel());

        assertEquals("57083005707", testClass.getPesel());
        assertTrue(extractDateMethod.canAccess(testClass));
        assertTrue(getGenderMethod.canAccess(testClass));
        assertTrue(validateMethod.canAccess(testClass));
        assertEquals(simpleDate.parse("30-08-1957"), expectedDate);
        assertEquals(Gender.Female, expectedGender);
        assertTrue((Boolean) expectedValidation);

    }


}